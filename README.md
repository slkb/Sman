# Sman


Sman是一个轻量级的web网络多线程爬虫框架，无需配置即可完成一个自定义的网络爬虫

> 本框架可以作为爬虫的入门学习，爬虫的基本原理
> 诸多不妥之处，请大神多多指教
> email: meng.fanding@qq.com

---

### demo
``` java
public static void main(String[] args) {
		
		Excavator.addSeed("http://www.ygdy8.net/");// 可添加多个种子
		/*
		 * 在这里设置访问深度、总数、工作规则类、工作线程数
		 */
		Excavator.start();// 开始挖掘
}
```

