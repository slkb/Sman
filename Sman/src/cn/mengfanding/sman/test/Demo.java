package cn.mengfanding.sman.test;

import java.net.UnknownHostException;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import cn.mengfanding.sman.crawler.Crawler;

public class Demo {

	private static int maxThread = 2;
	public static Queue<String> allPages = new LinkedList<String>();
	public static Queue<String> urls = new LinkedList<String>();// 准备爬取的url池
	public static Set<String> exUrls = new HashSet<String>();// 已经爬取了的url
	

	public static void main(String[] args) {
		
		ExecutorService pool = Executors.newFixedThreadPool(maxThread + 1);
		

		for (int i = 0; i < maxThread; i++) {
			Runnable t = new Runnable() {
				
				@Override
				public void run() {
					try {
						String html = Crawler.getHtmlByUrl("");
						allPages.offer(html);
						
					} catch (UnknownHostException e) {
						e.printStackTrace();
					} catch (IllegalArgumentException e) {
						e.printStackTrace();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			};
			pool.execute(t);
		}

	}

}
