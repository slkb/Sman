package cn.mengfanding.sman.test;

import java.net.URL;
import java.net.UnknownHostException;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

import org.jsoup.nodes.Document;

import cn.mengfanding.sman.crawler.Crawler;
import cn.mengfanding.sman.text.TextMatcher;

public class Demo1 {

	public static void main(String[] args) {
		Queue<String> pool = new LinkedList<String>();
		Set<String> old = new HashSet<String>();

		String _url = "http://www.ygdy8.net/html/gndy/dyzz/list_23_2.html";
		pool.offer(_url);
		while (true) {
			try {
				String nurl = pool.poll();
				if(old.contains(nurl)){
					System.out.println("已经访问过了");
					continue;
				}
				old.add(nurl);
				URL url = new URL(nurl);
				String html = Crawler.getHtmlByUrl(url.toString());
				Document doc = Crawler.getDocByUrl(url.toString());
//				doc.select("a").attr("href");
				Set<String> urls = TextMatcher.matcheSetInText("href=(\"|').+?html(\"|')", html);
				for (String u : urls) {
					// System.out.println(u);
					u = u.replace("href=\"", "").replace("\"", "").trim();
					try {
						new URL(u);
						// System.out.println("【有效】"+u);
						pool.offer(u);
					} catch (Exception e) {
						// System.out.println("【无效】"+u);
						// System.out.println(url.getHost()+u);
						String nu = url.getProtocol() + "://" + url.getHost() +"/"+ u;
						try {
							new URL(nu);
							// System.out.println("\t【有效】"+nu);
							pool.offer(nu);
						} catch (Exception e1) {
							// System.out.println("\t【无效】"+nu);
						}

					}

				}
				if (pool.size() > 300) {
					for (int i = 0; i < pool.size(); i++) {
						System.out.println(pool.poll());
					}
					return;
				}
			} catch (UnknownHostException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}
}
