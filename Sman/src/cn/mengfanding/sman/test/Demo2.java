package cn.mengfanding.sman.test;

import java.net.URI;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.LinkedList;
import java.util.Queue;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import cn.mengfanding.sman.crawler.Crawler;

public class Demo2 {

	Queue<String> readyUrl = new LinkedList<String>();
	
	
	public static void main(String[] args) {
		
		String url= "http://www.ygdy8.net";
		try {
			URI base=new URI(url);//基本网页URI  
			Document doc = Crawler.getDocByUrl(url);
			Elements es = doc.getElementsByTag("a");
			for (Element e : es) {
				System.out.println(e.attr("href"));
		        try {
					URI abs=base.resolve(e.attr("href"));//解析于上述网页的相对URL，得到绝对URI  
					URL absURL=abs.toURL();//转成URL  
					System.out.println("\t"+absURL);
				} catch (Exception e1) {
					e1.printStackTrace();
				}  
			}
			
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}
}
