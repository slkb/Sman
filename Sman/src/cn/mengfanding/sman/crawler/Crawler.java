package cn.mengfanding.sman.crawler;

import java.io.IOException;
import java.net.UnknownHostException;

import org.apache.log4j.Logger;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

/**
 * 爬虫类 负责爬取页面内容
 * 
 * @author 孟凡鼎
 *
 */
public class Crawler {

	// TODO 设置随机自动代理
	// TODO 设置爬虫深度
	// TODO 显示爬取时信息
	// TODO 如果防爬虫，设置userAgent

	private static Logger log = Logger.getLogger(Crawler.class);

	/**
	 * 根据url获取页面document文档对象
	 * 
	 * @param url
	 * @return
	 * @throws IOException
	 */
	public static Document getDocByUrl(String url) throws UnknownHostException, IllegalArgumentException, Exception {
		Document doc = null;
		try {
			Connection con=Jsoup.connect(url);
			con.header("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36");
			
			doc = con.get();
		} catch (Exception e) {
			log.error(url + " 连接异常");
			throw e;
		}
		return doc;
	}

	/**
	 * 根据url获取页面html字符串
	 * 
	 * @param url
	 * @return
	 * @throws IOException
	 */
	public static String getHtmlByUrl(String url) throws UnknownHostException, IllegalArgumentException, Exception {
		Document doc = getDocByUrl(url);
		if (doc != null)
			return doc.html();
		else
			return null;
	}

}
