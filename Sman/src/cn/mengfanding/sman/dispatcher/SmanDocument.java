package cn.mengfanding.sman.dispatcher;

import org.jsoup.nodes.Document;

public class SmanDocument {

	private String url;
	private Document document;
	
	
	

	public SmanDocument(String url, Document document) {
		this.url = url;
		this.document = document;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Document getDocument() {
		return document;
	}

	public void setDocument(Document document) {
		this.document = document;
	}

}
