package cn.mengfanding.sman.persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;

public abstract class DB {

	private static String driver;
	private static String url;
	private static String username;
	private static String password;
	private static Connection connection;
	private static Long timestemp = new Date().getTime();
	private static String createTableSQL="create table "
			+"SMAN_"+ timestemp+" "
					+ "("
					+ "text1 text"
					+ ")";
	private static PreparedStatement preapare;
	private static String insertSQL;

	static {
		try {
			Class.forName(driver);
			connection = DriverManager.getConnection(url, username, password);
			preapare = connection.prepareStatement(createTableSQL);
			preapare.executeUpdate();
			connection.setAutoCommit(false);
			preapare = connection.prepareStatement(insertSQL);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public void save(String... strings) {
		try {
			for (int i = 0; i < strings.length; i++) {
				String string = strings[i];
				preapare.setString(i + 1, string);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void flush() {
		
	}

}
