package cn.mengfanding.sman.text;

import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 字符串匹配检索类
 * 
 * 
 * @author 孟凡鼎
 *
 */
public class TextMatcher {

	/**
	 * 根据正则表达式从text中查找第一个匹配的值
	 * 
	 * @param regEx
	 * @param text
	 * @return
	 */
	public static String matchOneInText(String regEx, String text) {
		Pattern pat = Pattern.compile(regEx);
		Matcher mat = pat.matcher(text);
		if (mat.find()) {
			return (mat.group(0));
		}
		return null;
	}

	/**
	 * 根据正则表达式从text中查找所有匹配的字符串
	 * 
	 * @param regEx
	 * @param text
	 * @return
	 */
	public static Set<String> matcheSetInText(String regEx, String text) {
		Set<String> set = new HashSet<String>();
		Pattern pat = Pattern.compile(regEx);
		Matcher mat = pat.matcher(text);
		while (mat.find()) {
			set.add(mat.group(0));
		}
		return set;
	}

	/**
	 * 判断该字符串是否匹配
	 * 
	 * @param regEx
	 * @param text
	 * @return
	 */
	public static boolean matherRegText(String regEx, String text) {
		Pattern pattern = Pattern.compile(regEx, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(text);
		return matcher.matches();
	}
}
