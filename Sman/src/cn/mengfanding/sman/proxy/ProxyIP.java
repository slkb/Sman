package cn.mengfanding.sman.proxy;

/**
 * ����IP��
 * 
 * @author Administrator
 *
 */
public class ProxyIP {

	private String ip;
	private int port;

	public ProxyIP() {
	}

	public ProxyIP(String ip, int port) {
		super();
		this.ip = ip;
		this.port = port;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

}
